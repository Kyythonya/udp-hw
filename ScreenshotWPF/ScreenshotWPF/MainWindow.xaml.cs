﻿using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Windows;
using System.Windows.Media.Imaging;

namespace ScreenshotWPF
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly int _remotePort = 10002;
        private readonly int _localPort = 10001;
        private readonly string _localIp = "192.168.56.1";

        private UdpClient _client;

        public MainWindow()
        {
            InitializeComponent();

            try
            {
                _client = new UdpClient(new IPEndPoint(IPAddress.Parse(_localIp), _localPort));
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }
        }

        private void GetScreenshotButtonClick(object sender, RoutedEventArgs e)
        {
            ThreadPool.QueueUserWorkItem((state) =>
            {
                var ipEndPoint = new IPEndPoint(IPAddress.Any, _remotePort);

                try
                {
                    byte[] buffer = _client.Receive(ref ipEndPoint);
                    using (var stream = new MemoryStream(buffer, 0, buffer.Length))
                    {
                        stream.Write(buffer, 0, buffer.Length);
                        Dispatcher.Invoke(new Action(() =>
                        {
                            screenshot.Source = BitmapFrame.Create(stream, BitmapCreateOptions.None, BitmapCacheOption.OnLoad);
                        }), System.Windows.Threading.DispatcherPriority.Background);
                    }
                }
                catch (Exception exception)
                {
                    MessageBox.Show(exception.Message);
                }
            });
        }

        private void WindowClosing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            try
            {
                _client.Close();
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }
        }
    }
}
